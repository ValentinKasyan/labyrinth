package com.example.android.labyrinth;


import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

public class Exit extends Dot {
    Exit(Point point, int size) {
        super(point,getPaint(), size);
    }
    public static Paint getPaint(){// для обхода параметров конструктора создадим статическею функцию и вынесем в нее функционал
        Paint paint= new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.GREEN);
        return paint;
    }
}
