package com.example.android.labyrinth;


import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class GameManeger extends GestureDetector.SimpleOnGestureListener {
    //чтобы не имплементировать интерфейс GestureDetector и затем не реализовывать все его методы
    //унаследуемся от специального класса GestureDetector.SimpleOnGestureListener в котором
    // на всех методах стоят заглушки
    private List<Drawable> drawables= new ArrayList<>();
    private View view;
    private Player player;
    private Exit exit;
    private Maze maze;
    //для центрального расположение лабиринта на любых экранах
    // будем узнавать реальные размеры используемых экрана устройств
    private Rect rect=new Rect();
    private int screenSize ;


    GameManeger(){
        create(5);


    }
    public void create(int size){
        drawables.clear();
        maze=new Maze(size);
        player=new Player(maze.getStart(), size);
        // добавляем элименты в отрисовку
        drawables.add(maze);
        drawables.add(player);
        exit = new Exit(maze.getEnd(), size);
        drawables.add(exit);
    }
    public void draw(Canvas canvas){
        // проходимся цыклом по всем елементам коллекции и отрисовываем их
        for (Drawable drawableitem:
            drawables ) {
            drawableitem.draw(canvas,rect);
        }
    }

    public void setView(View view) {
        this.view = view;
    }

    //реализация метода в классе GestureDetector.SimpleOnGestureListener
    //  для работы с ним в MainActivity
    // в качестве параметров передается начало касания e1, конец косания e2, ускорение по х,у
    //обработчик жестов
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        // для определения в какую сторону был сделан Swipe
        int difFx;
        int difFy;
        difFx = Math.round(e2.getX() - e1.getX());//округляем число до целого
        difFy = Math.round(e2.getY() - e1.getY());
        //определяем в какую сторону было сделан смах
        if (Math.abs(difFx) > Math.abs(difFy)) {
            difFy=0;
            if (difFx>0){
                difFx=1;
            }else if (difFx<0){
                difFx=-1;
            }else if (difFx==0){
                Log.i("Dif",String.format("difFx=0"));
            }

        } else {
            difFx=0;
            if (difFy>0){
                difFy=1;
            }else if (difFy<0){
                difFy=-1;
            }else if (difFy==0){
                Log.i("Dif",String.format("difFy=0"));
            }

        }
        int stepX=player.getX();
        int stepY=player.getY();
        while (maze.canPlayerGoTo(stepX+difFx,stepY+difFy)){
            stepX+=difFx;
            stepY+=difFy;
            if (difFx!=0){
                if (maze.canPlayerGoTo(stepX,stepY+1)||maze.canPlayerGoTo(stepX,stepY-1)){
                    break;
                }
            }if (difFy!=0){
                if (maze.canPlayerGoTo(stepX+1,stepY)||maze.canPlayerGoTo(stepX-1,stepY)){
                    break;
                }
            }
        }

        player.goTo(stepX,stepY);//реализуем перемещение
        if (exit.getPoint().equals(player.getPoint())){

            create(maze.getSize()+5);
        }



        view.invalidate();//перерисовка обязательно для вызова для отрисовки
     //Log.i("Dif",String.format("difFx=%f",difFx)+"    "+String.format("difFy=%f",difFy));
        return super.onFling(e1, e2, velocityX, velocityY);
    }
    //метод для получения габаритов экрана для точного расположения лабиринта
    public void setScreenSize(int width,int height){
        screenSize =Math.min(width, height);// находим минимальную сторону
        //передаем координаты верхнего левого угла отрисовки картинки зная минимальную сторону
        rect.set((width- screenSize)/2,
                (height- screenSize)/2,
                (width+ screenSize)/2,
                (height+ screenSize)/2);
    }
}
