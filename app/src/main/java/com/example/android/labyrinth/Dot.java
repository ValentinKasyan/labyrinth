package com.example.android.labyrinth;


import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;

public class Dot implements Drawable {


    protected Point point;//будем хранить координаты нашей точки
    protected Paint paint;
    protected  int size;

    Dot(Point point,Paint paint,int size){
        this.point=point;
        this.paint=paint;
        this.size=size;
    }

    public Point getPoint() {
        return point;
    }

    @Override
    public void draw(Canvas canvas,Rect rect) {
        //отрисовка пользовательского квадрата
        float cellSize = (float) (rect.right - rect.left) /size;

        canvas.drawRect(
                rect.left + point.x*cellSize,
                rect.top + point.y*cellSize,
                rect.left + point.x*cellSize+cellSize,
                rect.top + point.y*cellSize+cellSize,
                paint);

    }
}
