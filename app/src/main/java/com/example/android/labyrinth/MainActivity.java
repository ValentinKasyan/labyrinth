package com.example.android.labyrinth;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private MazeView view;
    private GestureDetector gestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GameManeger gameManeger=new GameManeger();
        view=new MazeView(this,gameManeger);
        setContentView(view);

        //для реализации различных жестов и определения разных вариантав нажатия на экран
        // реализован в GameManeger через перегруженный
        // метод onFling класса GestureDetector.SimpleOnGestureListener
        gestureDetector = new GestureDetector(this,gameManeger);

    }
//отлавливает все события по нажатию на экран
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //для реализации различных жестов и определения разных вариантав нажатия на экран
        return gestureDetector.onTouchEvent(event);
    }
}
