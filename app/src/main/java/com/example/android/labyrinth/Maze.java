package com.example.android.labyrinth;


import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Stack;

public class Maze implements Drawable {
    private Paint wallPaint;
    private final boolean arr [][];
    private final int size;
    private final Point end=new Point(1,1);
    private Point start;
    private int bestScore=0;//самый длинный путь

    public Maze(int size){
        this.size = size;
        //параметры отрисовки
        wallPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        wallPaint.setColor(Color.BLACK);
        arr=new boolean[size][size];
        generateMaze();
    }

    public Point getEnd() {
        return end;
    }

    public Point getStart() {
        return start;
    }
    public int getSize() {
        return size;
    }


    // функция для реализации массива значениями
    private void generateMaze() {
        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                arr[i][j] = i % 2 != 0 && j % 2 != 0 && i < size - 1 && j < size - 1;
            }
        }

        Random random = new Random();
        //добавили стек точек в которую будем добавлять
        Stack<Point> stack = new Stack<>();
        //размещаем первую точку
        stack.push(end);
        while (stack.size() > 0) {
            Point current = stack.peek();//извлекать значения , но не удалять их
            // создадим список соседей
            List<Point> unusedNeighbors = new LinkedList<>();
            // проверка соседней клетки на свободность,пойти влево
            if (current.x > 2) {
                //не используется ли клетка, свободен не стенка
                if (!isUsedCell(current.x-2,current.y)){
                    unusedNeighbors.add(new Point(current.x-2,current.y));
                }
            }
            if (current.y > 2) {// пойти вверх
                //не используется ли клетка, свободен не стенка
                if (!isUsedCell(current.x,current.y-2)){
                    unusedNeighbors.add(new Point(current.x,current.y-2));
                }
            }
            if (current.x <size- 2) {
                //не используется ли клетка, свободен не стенка
                //используется ли клетка вправо через 1 , по тому же у
                if (!isUsedCell(current.x+2,current.y)){
                    unusedNeighbors.add(new Point(current.x+2,current.y));
                }
            }
            if (current.y <size- 2) {
                //не используется ли клетка, свободен не стенка
                if (!isUsedCell(current.x,current.y+2)){
                    unusedNeighbors.add(new Point(current.x,current.y+2));
                }
            }
            //если у нас есть свободные клетки вокруг , то случайным образом выбирается номер клетки для [0;size)
            if (unusedNeighbors.size()>0){
                int rand= random.nextInt(unusedNeighbors.size());
                //выбираем случайное направление
                Point direction = unusedNeighbors.get(rand);
                //получаем координаты следующей точки
                int difFx=(direction.x-current.x)/2;
                int difFy=(direction.y-current.y)/2;
                arr[current.y+difFy][current.x+difFx]=true;//снос стенки
                stack.push(direction);
            }else {
                if (bestScore < stack.size()) {
                    bestScore = stack.size();
                    start = current;
                }
                stack.pop();//если вокруг клетки нет ни одного направления , она выбрасывается
            }
        }
    }
        private boolean isUsedCell(int x, int y){
        if(x<0||y<0||x>=size-1||y>=size-1)// такой ячейки не существует
        //где х -это вертикаль с направлением вниз, у-это горизонталь с направлением слево на право
        { return true;}

        return arr[y-1][x] //левая
                ||arr[y][x-1]//верхняя
                ||arr[y+1][x]//правая
                ||arr[y][x+1];//нижняя
    }



    @Override
    public void draw(Canvas canvas, Rect rect) {
        //отрисовка лабиринта,узнаем размер ячейки
        float cellSize = (float) (rect.right - rect.left) / size;
        for (int i=0; i < size; ++i) {
            for (int j=0; j < size; ++j) {
                if (!arr[i][j]) {//для отрисовки квадрата
                    float left = j * cellSize + rect.left;
                    float top = i * cellSize + rect.top;
                    canvas.drawRect(left,
                            top,
                            left+cellSize,
                            top+cellSize,
                            wallPaint);
                }
            }
        }


    }
    public boolean isCrossroad(int x, int y){
        return isUsedCell(x,y);
    }

    //может ли пользователь двигаться в направлении клетки

    public boolean canPlayerGoTo(int x, int y){
        return  arr[y][x];
    }
}
