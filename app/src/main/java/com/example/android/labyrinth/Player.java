package com.example.android.labyrinth;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;

import static android.os.Build.VERSION_CODES.M;


public class Player extends Dot {

    public Player(Point start, int size){// в конструкторе как параметр задаем точку старта для игрока
        //параметры отрисовки
        super(start,getPaint(),size);
        point=start;
        this.size=size;
    }
    public static Paint getPaint(){// для обхода параметров конструктора создадим статическею функцию и вынесем в нее функционал
        Paint paint= new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.BLUE);
        return paint;
    }

    public int getX() {
        return point.x;
    }
    public int getY() {
        return point.y;
    }

    public void goTo (int x, int y){
        point.x= x;
        point.y= y;
    }

}
