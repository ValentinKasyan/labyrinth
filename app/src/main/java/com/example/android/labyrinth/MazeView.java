package com.example.android.labyrinth;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;


//класс в котором осуществляется окончательная отрисовка в методе onDraw() класс ,
// работающий с отображением extends View
public class MazeView extends View {
    private GameManeger gameManeger;
    public MazeView(Context context,GameManeger gameManeger) {
        super(context);
        this.gameManeger=gameManeger;
        gameManeger.setView(this);// может перерисовать View
    }

    @Override
    protected void onDraw(Canvas canvas) {
        gameManeger.draw(canvas);

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        gameManeger.setScreenSize(w,h);
    }
}
